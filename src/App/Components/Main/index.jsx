import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { setScreen } from 'App/Reducers/Screen/screenActions';

import ContentWrapper from 'App/Components/Wrapper'
import Cabinet from 'App/Components/Cabinet'
import Pokedex from 'App/Components/Pokedex'
import Display from 'App/Components/Display'


class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mounted: false
        }

        this.updateDimensions = this.updateDimensions.bind(this);
    }

    componentWillMount() {
        // console.log(this.props.screen)
        window.addEventListener("resize", this.updateDimensions, false);        
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions, false);
    }

    componentDidMount() {
        this.setState({ mounted: true})
    }

    updateDimensions () {
        this.props.setScreen({
            landscape: window.innerWidth >= window.innerHeight,
            width: window.innerWidth,
            height: window.innerHeight
        })
        this.forceUpdate()
    }

    shouldComponentUpdate(props, state) {
        return props.screen !== this.props.screen || !this.state.mounted
    }
    
    render() {
        return (
            <div id="Main">
                <div className="overlay"></div>
                <ContentWrapper content = {[Display, Pokedex]}/>
            </div>
        )
    }
}

const StateToProps = state => ({ ...state })
const DispatchToProps = dispatch => 
    bindActionCreators({ setScreen }, dispatch)
export default connect(StateToProps, DispatchToProps)(Main)

