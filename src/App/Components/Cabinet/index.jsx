import React, { Component } from 'react';

import appData from 'App/Data/appData'
import getData from 'App/Services/getData'
import Pot from 'App/Components/Pot'
import Button from 'App/Components/Button'

class Cabinet extends Component {
	constructor(props) {
		super(props)
		this.state = {
			potList: [],
			activePot: 'brass',
		}
	}

	componentWillMount(){
		getData({
			method: 'get',
			url: appData.api.pots
		}, potList => this.setState({ potList }))
	}

	changePot(activePot){
        this.setState({
            ...this.state,
            activePot
        })
    }

	render() {
		return (
			<div id="cabinet">
				<Pot active={this.state.activePot}/>
				<div className="pot-controllers">
					{this.state.potList.map((pot, i) => {
						return(
							<Button key = {i} className = {pot.name.toLowerCase() + "-button change-button"} 
								onClick={this.changePot.bind(this, pot.name.toLowerCase())}/>
						)
					})}
				</div>
			</div>
		);
	}
}

export default Cabinet;
