import React, { Component } from 'react';

class Pot extends Component {

    constructor(props) {
        super(props);

        this.state = {
        }
    }

    renderSlots() {
        let slots;
        return slots
    }

    componentWillMount() {
    }

    render() {
        return (
            <div id="potContainer" className={this.props.active + "-pot pot_container"}>
                <div className="pot_cover-shoulder_strap">
                    <div className="pot_cover-shoulder_strap-brights">
                        <div className="pot_bright"></div>
                    </div>
                </div>

                <div className="pot_cover">
                    <div className="pot_cover-brights">
                        <div className="pot_bright"></div>
                        <div className="pot_bright"></div>
                    </div>
                </div>

                <div className="pot_base">

                    <div className="pot_base-slots">
                        <div className="pot_slot"></div>
                        <div className="pot_slot"></div>
                        <div className="pot_slot"></div>
                        <div className="pot_slot"></div>
                        <div className="pot_slot"></div>
                    </div>
                    
                    <div className="pot_base-border-brights">
                        <div className="pot_bright"></div>
                        <div className="pot_bright"></div>
                        <div className="pot_bright"></div>
                    </div>

                    <div className="pot_base-shoulder_strap left-strap"></div>
                    <div className="pot_base-shoulder_strap right-strap"></div>

                </div>

                <div className="pot_base-details">
                    <div className="pot_detail"></div>
                    <div className="pot_detail"></div>
                    <div className="pot_detail"></div>
                    <div className="pot_detail"></div>
                    <div className="pot_detail"></div>
                </div>

                <div className="pot_base-brights">
                    <div className="pot_bright"></div>
                    <div className="pot_bright"></div>
                </div>
            </div>
        );
    }
}

export default Pot;
