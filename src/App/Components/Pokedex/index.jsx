import React, { Component } from 'react';
import Pokethumb from 'App/Components/Thumb';
import getData from 'App/Services/getData';
import CustomScroll from 'react-custom-scroll';

class Pokedex extends Component {
    constructor(props) {
        super(props);

        this.state = {
            list: []
        }            
    }

    componentWillMount(){
        getData({
            method: 'post',
            url: 'http://localhost:3003/api/pokemon',
            data: {
                dropable: false
            }
        }, list => {
            this.setState({ list })
        })
    }

    render() {
        
        return (
            <div id="pokedex">
                <CustomScroll allowOuterScroll={true} >
                    <div className="monster-container">
                        {this.state.list.map((item, key) => {
                            let props = {
                                dexid: item.dexID,
                                name: item.name[0].toUpperCase() + item.name.slice(1).replace(/\-([a-z]{1})(\w*)/, (match, $1, $2) => " "+$1.toUpperCase()+$2),
                                key
                            }
                        
                            return <Pokethumb {...props}/>
                        })}
                    </div>
                </CustomScroll>
            </div>
        );
    }
}

export default Pokedex;
