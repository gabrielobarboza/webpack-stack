import * as THREE from 'three'
import FBXLoader from 'three-fbx-loader'
import GLTF2Loader from 'three-gltf2-loader'

import colors from 'Assets/styles/_colors.scss' 

GLTF2Loader(THREE)

let canvas = {}

const start = (callback) => {
    canvas.lastUpdate = Date.now()
    
    if (!canvas.frameId) {
        canvas.frameId = requestAnimationFrame(canvas.animate)
    }

    return callback && callback(canvas)
}	
const stop = () => {
    cancelAnimationFrame(canvas.frameId)
}
	
const animate = () => {
    const update = Date.now()
    const dt = update - canvas.lastUpdate
    canvas.lastUpdate = update

    if(canvas.scene) {
        canvas.scene.getObjectByName('monster').rotation.y += 0.01
        canvas.scene.getObjectByName('monster').mixer.update(dt/1000)
    }

    canvas.renderScene()
    canvas.frameId = window.requestAnimationFrame(canvas.animate)

}	
const renderScene = () => {
    canvas.renderer ? canvas.renderer.render(canvas.scene, canvas.camera) : null
}


function threeCanvas ( params, callback) {
    if(!params.selected) return 

    const scene = new THREE.Scene()
    // const loader = new FBXLoader()
    const loader = new THREE.GLTFLoader()
    const camera = new THREE.PerspectiveCamera(75, 1, 0.1, 1000)
    const light = new THREE.DirectionalLight(0xffffff, 0.2)
    const renderer = new THREE.WebGLRenderer({ antialias: true })
    const material = new THREE.MeshLambertMaterial({ vertexColors: THREE.VertexColors })

    const filename = `PM${params.selected}.glb`
    let monster = null
    
    canvas = { start, stop, animate, renderScene }
    
    loader.load(`/assets/meshes/gltf/${filename}`, (model) => {
        
        monster = model.scene.children[0].children[0]
        monster.name = "monster"
        monster.mixer = new THREE.AnimationMixer(monster)
        monster.material = material

        monster.position.y = -2
        monster.position.z = -2
        monster.scale.set(1, 1, 1) 

        camera.position.z = 5

        light.position.set(1, 1, 1)
        light.castShadow = true

        scene.add(new THREE.AmbientLight(colors.black))
        scene.add(monster)
        scene.add(light)
        scene.add(light.target)
        light.target = monster

        renderer.setClearColor(colors.dark)
        renderer.setSize(params.size, params.size)

        let monsterAction = model.animations.filter(animation => (/M\d*_\w*/).test(animation.name))
        if(monsterAction.length) monster.mixer.clipAction(monsterAction[0]).play()
            
        let defaultAction = model.animations.filter(animation => (/Mmotion_idle_\w*/).test(animation.name))
        if(defaultAction.length) monster.mixer.clipAction(defaultAction[0]).play()
        
        canvas = { ...canvas, scene , camera , renderer , material , monster, filename }
        canvas.start(callback)

    }, (xhr)=>{
        //loading routine
    }, (err) => {
        console.error("Error: ", err)
    })


}

module.exports = threeCanvas