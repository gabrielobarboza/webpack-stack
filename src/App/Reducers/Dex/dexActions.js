export const selectMonster = (id) => {
    return dispatch => {
        dispatch({ type: 'SELECT_MONSTER', id })
    }
}