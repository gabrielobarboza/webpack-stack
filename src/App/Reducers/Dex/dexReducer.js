const DEX_INITIAL_STATE = { selected: 1 }

export default (state = DEX_INITIAL_STATE, action) => {
    switch(action.type) {
        case 'SELECT_MONSTER':

            state.selected = action.id

            return {...state}
            
        default:
            return state
    }
}