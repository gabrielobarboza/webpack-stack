import screenReducer from './Screen/screenReducer';
import dexReducer from './Dex/dexReducer';

import { combineReducers } from 'redux'

const rootReducer = combineReducers({
    screen : screenReducer,
    dex : dexReducer
})

export default rootReducer